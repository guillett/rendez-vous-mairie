# Bienvenue sur le projet ANTS rendez-vous mairie  👋
Le projet rendez vous mairie est une application web qui vous permet de chercher et réserver des créneaux disponibles dans une mairie à pproximité pour déposer des dossiers de demande des titres.

Cette application va être utilisée par les citoyens francais.
## :pencil: Requirements

Vous trouverez ici les requirements du back-end:

[![python official](https://img.shields.io/static/v1?label=Python&color=FFE873&message=3.10.2&style=flat&logo=Python&labelColor=306998&logoColor=fff)](https://www.python.org/downloads/release/python-3102/)
[![fastAPI official](https://img.shields.io/static/v1?color=grey&message=0.79.0&style=flat&label=FastAPI&labelColor=000&logo=FastAPI&logoColor=fff)](https://fastapi.tiangolo.com/)
[![gunicorn official](https://img.shields.io/static/v1?color=grey&message=0.18.2&style=flat&label=Gunicorn&labelColor=138b44&logo=gunicorn&logoColor=fff)](https://www.uvicorn.org/)

Vous trouverez ici les requirements du frontend:

[![node official](https://img.shields.io/static/v1?label=Node&color=grey&message=16.16.0&style=flat&logo=node.js&labelColor=3c873a&logoColor=fff)](https://nodejs.org/en/)
[![npm official](https://img.shields.io/static/v1?label=npm&color=grey&message=8.11.0&style=flat&loge=npm&labelColor=1563FF)](https://docs.npmjs.com/)
[![angular official](https://img.shields.io/static/v1?color=grey&message=14.0.5&style=flat&label=Angular&labelColor=FF0000&logo=Angular&logoColor=fff)](https://angular.io/)

Pipeline

[![gitlab official](https://img.shields.io/static/v1?color=fca326&message=14.0.5&style=flat&label=Gitlab&labelColor=grey&logo=Gitlab&logoColor=fff)](https://gitlab.io/)
[![gitlab official](https://img.shields.io/static/v1?color=fca326&message=%&style=flat&label=JScoverage&labelColor=greylogo=Gitlab)](https://gitlab.io/)
[![gitlab official](https://img.shields.io/static/v1?color=fca326&message=%&style=flat&label=pythonCoverage&labelColor=greylogo=Gitlab)](https://gitlab.io/)



API Rest développée avec le framework FastAPI.

## Structure de projet
```
📦rendez-vous-mairie
 ┣ 📂.gitlab
 ┣ 📜.gitlab-ci.yml
 ┣ 📜.gitignore
 ┣ 📜requirements-dev.txt
 ┣ 📜.flake8
 ┗ 📂hub-rdv-backend
   ┣ 📜dispatch.yaml
   ┣ hub-rd-backend
   ┃ ┣ ...
   ┃ ┣ 📜requirements.txt
   ┃ ┣ 📜main.py
   ┃ ┗ 📜Procfile
   ┗ 📂hub-rdv-frontend
     ┣ ...
     ┣ 📜package.json
     ┣ 📜.prettierrc
     ┣ 📜.stylelintrc
     ┣ 📜.eslintrc.json
     ┣ 📜.prettierignore
     ┣ 📜server.js
     ┗ 📜Procfile
```

Please follow [KISS](https://en.wikipedia.org/wiki/KISS_principle) principles
- 📦rendez-vous-mairie : Le projet de base
- 📂.gitlab : gitlab ci et trucs de modèle
- 📜.gitlab-ci.yml : La configuration gitlab ci
- 📂hub-rdv-backend: backend en Python 
- 📂hub-rdv-frontend: application frontale dans angular 


## 🔧 Development
Install dependencies and configuration

### Backend

```bash
# Allez au dossier rendez-vous mairie
pip install -r requirements-dev.txt
# Allez au dossier hub-rdv-backend
cd hub-rdv-backend
# install python dependencies and dev dependencies
pip install -r requirements.txt
```

### Frontend

```bash
# Allez au dossier hub-rdv-frontend
cd hu-rdv-frontend
# Installer les dépendances de node
npm install
```

### Run it

#### Run front in local
```bash
# Allez au dossier hub-rdv-frontend
cd hu-rdv-frontend
# Installer les dépendances de node
npm start
```
#### Run backend
- Ajouter le fichier .env au racine de projet backned
```bash
# Allez au dossier hub-rdv-frontend
cd hu-rdv-backend
# Créer la fichier .env
touch .env
# copy la configuration suivante
AUTH_TOKENS=["test-token-1", "test-token-2"]
ENVIRONMENT='dev'
MOCK_EDITORS=False
MOCK_EMPTY_RESPONSE=False
HOST='0.0.0.0'
PORT='8081'
citopia_auth_token='secret'
synbird_auth_token='secret'
# Lancer le backend
python main.py
```
## :warning:️ testing
Pour exécuter des tests backend, assurez-vous d'installer les fichiers `requirements-dev.txt`.

```shell
# Allez au dossier hub-rdv-backend
 cd hub-rdv-backend
 # Exécuter la cmd
 pytest
```
#### test frontend
```shell
# Allez au dossier hub-rdv-frontend
 cd hub-rdv-frontend
# Exécuter la cmd
 npm test
```

## :warning:️ linting
### backend
Pour garder le format du code, assurez-vous d'installer les fichiers `requirements-dev.txt`.

```shell
# Allez au dossier hub-rdv-backend
 flake8 hub-rdv-backend
 cd hub-rdv-backend
 # Exécuter
 black .
 isort --profile black .
```
pour verifier si le format et le style et bon
```shell
# Allez au dossier hub-rdv-backend
 flake8 hub-rdv-backend
 cd hub-rdv-backend
 # Exécuter
 black --check .
 isort --check.
```
### Frontend
```shell
 cd hub-rdv-frontend
 # pour corriger le lint et le style automatiquement
 npm run format
 # pour vérifier le lint et le style
  npm run lint
```
## URLs of the APP:

|environnement |url front  |url swagger  |url api  |
|-----------|------|------|------|
|local| http://127.0.0.1:4200|http://127.0.0.1:8080/docs|http://127.0.0.1:8081/api |
|production| https://hub-rdv-frontend.osc-fr1.scalingo.io/|todo| todo|

