from datetime import date, timedelta

search_criteria = {
    "latitude": 48.8717479,
    "longitude": 2.357835,
    "start_date": date.today(),
    "end_date": date.today() + timedelta(90),
    "radius_km": 40,
}
