from src.services.mock_data import get_mock_slots

list_meeting_points = [
    {
        "id": "201",
        "name": "Mairie ANNEXE LILLE-SECLIN",
        "longitude": 3.0348016639327,
        "latitude": 50.549140395451,
        "_internal_id": "123123",
        "public_entry_address": "89 RUE ROGER BOUVRY 59113 SECLIN",
        "zip_code": "59113",
        "city_name": "LILLE-SECLIN",
        "website": "http://www.ville-seclin.fr",
        "city_logo": "https://www.ville-seclin.fr/images/logo-ville-seclin/logo_ville_de_seclin.png",
        "_editor_name": "TestEditor",
    },
    {
        "id": "202",
        "name": "Mairie de Quartier de Lille-Sud",
        "longitude": 3.0475818403133,
        "latitude": 50.612875943839,
        "_internal_id": "456456",
        "public_entry_address": "83 Rue du Faubourg des Postes",
        "zip_code": "59000",
        "city_name": "LILLE-SECLIN",
        "website": "http://www.lille.fr/Lille-Sud2/Mairie-de-quartier-de-Lille-Sud",
        "city_logo": "https://www.ville-seclin.fr/images/logo-ville-seclin/logo_ville_de_seclin.png",
        "_editor_name": "TestEditor",
    },
    {
        "id": "203",
        "name": "Mairie du 10e Arrondissement de Paris",
        "longitude": 2.357828,
        "latitude": 48.8717442,
        "_internal_id": "789879",
        "public_entry_address": "72 Rue du Faubourg Saint-Martin",
        "zip_code": "75010",
        "city_name": "Paris",
        "website": "https://mairie10.paris.fr",
        "city_logo": "https://www.grapheine.com/wp-content/uploads/Plan-de-travail-36paris-logo.jpg",
        "_editor_name": "TestEditor",
    },
]

meeting_points_with_time_slots = list_meeting_points.copy()
for meeting_point in meeting_points_with_time_slots:
    meeting_point["available_slots"] = get_mock_slots(
        meeting_point,
    )
