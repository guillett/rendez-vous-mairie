from datetime import date, timedelta

from fastapi.testclient import TestClient
from src.db.utils import get_all_meeting_points, set_all_editors, set_all_meeting_points
from src.services.mock_data import get_mock_slots_multiple
from tests.mocks import editor as editor_mock
from tests.mocks import meeting_point as meeting_points_mock


def test_slots_from_position_ok(mocker, client: TestClient):
    set_all_editors([editor_mock.test_editor])
    set_all_meeting_points(meeting_points_mock.list_meeting_points)
    mocker.patch(
        "src.models.editor.Editor.get_available_time_slots",
        return_value=get_mock_slots_multiple(
            [get_all_meeting_points()[2]], date.today(), date.today() + timedelta(90)
        ),
    )
    parameters = {
        "latitude": 48.8717479,
        "longitude": 2.357835,
        "start_date": date.today(),
        "end_date": date.today() + timedelta(90),
        "radius_km": 40,
    }
    response = client.get("/api/SlotsFromPosition", params=parameters)
    json_response = response.json()
    assert isinstance(json_response, list)
    assert len(json_response) == 1


def test_slots_from_position_missing_params(mocker, client: TestClient):
    set_all_editors([editor_mock.test_editor])
    set_all_meeting_points(meeting_points_mock.list_meeting_points)
    mocker.patch(
        "src.models.editor.Editor.get_available_time_slots",
        return_value=get_mock_slots_multiple(
            [get_all_meeting_points()[2]], date.today(), date.today() + timedelta(90)
        ),
    )
    parameters = {
        "latitude": None,
        "longitude": 2.357835,
        "start_date": None,
        "end_date": date.today() + timedelta(90),
        "radius_km": 40,
    }
    response = client.get("/api/SlotsFromPosition", params=parameters)
    assert response.status_code == 422
    json_response = response.json()
    assert isinstance(json_response, dict)
    assert len(json_response["detail"]) == 2
    assert json_response["detail"][0]["type"] == "value_error.missing"


def test_slots_from_position_bad_param_type(mocker, client):
    set_all_editors([editor_mock.test_editor])
    set_all_meeting_points(meeting_points_mock.list_meeting_points)
    mocker.patch(
        "src.models.editor.Editor.get_available_time_slots",
        return_value=get_mock_slots_multiple(
            [get_all_meeting_points()[2]], date.today(), date.today() + timedelta(90)
        ),
    )
    parameters = {
        "latitude": 48.8717479,
        "longitude": 2.357835,
        "start_date": date.today(),
        "end_date": date.today() + timedelta(90),
        "radius_km": "someText",
    }
    response = client.get("/api/SlotsFromPosition", params=parameters)
    assert response.status_code == 422
    json_response = response.json()
    assert isinstance(json_response, dict)
    assert len(json_response["detail"]) == 1
    assert json_response["detail"][0]["type"] == "type_error.integer"
