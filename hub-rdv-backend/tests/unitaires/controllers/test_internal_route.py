from unittest.mock import ANY

import pytest
import src.controllers.routes.internal as routers
from fastapi import HTTPException
from pytest_mock import MockerFixture
from starlette.status import HTTP_400_BAD_REQUEST
from tests.mocks import municipality as mun


def test_meeting_points_from_position_ok(mocker: MockerFixture) -> None:
    """
    test the route /MeetingPointsFromPosition  when we send the good params.
    """
    mocker_get_all_meeting_points = mocker.patch(
        "src.controllers.routes.internal.get_all_meeting_points",
        return_value=mun.list_municipality,
    )
    mocker_search_close_meeting_points = mocker.patch(
        "src.controllers.routes.internal.search_close_meeting_points",
        return_value=mun.list_municipality_with_distance,
    )
    response = routers.meeting_points_from_position(
        ANY, longitude=2.352222, latitude=48.856613, radius_km=15
    )
    assert response == mun.list_municipality_with_distance
    mocker_get_all_meeting_points.assert_called_once()
    mocker_search_close_meeting_points.assert_called_once_with(
        mun.list_municipality, 48.856613, 2.352222, 15
    )


def test_meeting_points_from_position_HTTP_400_BAD_REQUEST(
    mocker: MockerFixture,
) -> None:
    """
    test route /MeetingPointsFromPosition  when we send the bad params.
    """
    mocker_get_all_meeting_points = mocker.patch(
        "src.controllers.routes.internal.get_all_meeting_points",
        return_value=mun.list_municipality,
    )
    mocker_search_close_meeting_points = mocker.patch(
        "src.controllers.routes.internal.search_close_meeting_points",
        side_effect=HTTPException(
            status_code=HTTP_400_BAD_REQUEST, detail="Bad type of params"
        ),
    )
    with pytest.raises(HTTPException) as excep:
        routers.meeting_points_from_position(
            ANY, longitude="badparams", latitude=48.856613, radius_km=15
        )
    assert excep.value.status_code == HTTP_400_BAD_REQUEST
    assert excep.value.detail == "Bad type of params"
    mocker_get_all_meeting_points.assert_called_once()
    mocker_search_close_meeting_points.assert_called_once_with(
        mun.list_municipality, 48.856613, "badparams", 15
    )
