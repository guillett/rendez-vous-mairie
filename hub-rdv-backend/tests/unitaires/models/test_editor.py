from datetime import date, timedelta

import pytest
from src.services.mock_data import get_mock_slots_multiple
from tests.mocks import editor as editor_mock
from tests.mocks import meeting_point as meeting_points_mock


@pytest.mark.asyncio
async def test_search_slots_in_editor(mocker):
    meeting_points = [meeting_points_mock.list_meeting_points[2]]
    start_date = date.today()
    end_date = date.today() + timedelta(90)
    mocker.patch(
        "src.models.editor.Editor.get_available_time_slots",
        return_value=get_mock_slots_multiple(meeting_points, start_date, end_date),
    )
    result = await editor_mock.test_editor.search_slots_in_editor(
        meeting_points,
        start_date=start_date,
        end_date=end_date,
    )
    assert len(result) == 1
    assert result[0]["id"] == "203"
    assert len(result[0]["available_slots"]) > 0
