import os

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.tasks import repeat_every
from src.controllers.controller import router as api_router
from src.core.config import get_settings
from src.db.utils import set_all_editors
from src.models.editor import init_all_editors
from src.services.scheduled_functions import run_daily_scheduled_functions

load_dotenv()


def get_application() -> FastAPI:
    application = FastAPI(title=get_settings().project_name)
    application.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    application.include_router(api_router, prefix=get_settings().api_prefix)
    return application


app = get_application()

# init the list of editors
set_all_editors(init_all_editors())


@app.on_event("startup")
@repeat_every(seconds=60 * 60 * 24)
def daily_scheduled_functions():
    run_daily_scheduled_functions()


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=os.environ.get("HOST"),
        port=int(os.environ.get("PORT")),
        reload=True,
    )
