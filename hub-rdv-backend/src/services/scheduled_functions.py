import logging

from src.db.utils import get_all_editors, set_all_meeting_points

_logger = logging.getLogger(__name__)


def run_daily_scheduled_functions():
    try:
        meeting_point_list = []
        for editor in get_all_editors():
            print(f"Getting Meeting Points list from editor {editor.name} ...")
            meeting_point_list += editor.get_managed_meeting_points()
        point_index = 201
        for point in meeting_point_list:
            point["id"] = str(point_index)
            point_index += 1
        set_all_meeting_points(meeting_point_list)
    except Exception as daily_sch_e:
        print(f"Error while running daily scheduled functions : {daily_sch_e}")
