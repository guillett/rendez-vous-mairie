import json
from datetime import datetime

from fastapi import APIRouter, WebSocket
from src.controllers.routes.time_slots import search_slots

router = APIRouter()


@router.websocket("/SlotsFromPositionStreaming")
async def slots_from_position_streaming(websocket: WebSocket):
    await websocket.accept()
    while True:
        raw_data = await websocket.receive_text()
        data = json.loads(raw_data)
        latitude = float(data["latitude"])
        longitude = float(data["longitude"])
        start_date = datetime.strptime(data["start_date"], "%Y-%m-%d").date()
        end_date = datetime.strptime(data["end_date"], "%Y-%m-%d").date()
        radius_km = int(data["radius_km"])

        await search_slots(
            longitude, latitude, start_date, end_date, radius_km, websocket=websocket
        )

        await websocket.send_text("end_of_search")
