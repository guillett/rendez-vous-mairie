import asyncio
import os
from datetime import date
from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException, Query
from pydantic import Required
from src.controllers.dependencies.auth_token import verify_auth_token
from src.db.utils import get_all_editors, get_all_meeting_points
from src.models.municipality import Municipality
from src.services.mock_data import get_mock_slots

router = APIRouter()


@router.get(
    "/getManagedMeetingPoints",
    response_model=List[Municipality],
    responses={
        200: {
            "description": "Municipalities list retrieved successfully!",
            "content": {
                "application/json": {
                    "example": [
                        {
                            "id": "201",
                            "name": "Mairie ANNEXE LILLE-SECLIN",
                            "longitude": 3.0348016639327,
                            "latitude": 50.549140395451,
                            "public_entry_address": "89 RUE ROGER BOUVRY 59113 SECLIN",
                            "zip_code": "59113",
                            "city_name": "SECLIN",
                            "website": "http://www.ville-seclin.fr",
                            "city_logo": "https://www.ville-seclin.fr/images/logo-ville-seclin/logo_ville_de_seclin.png",
                        }
                    ]
                }
            },
        }
    },
    dependencies=[Depends(verify_auth_token)],
)
def get_managed_meeting_points() -> Any:
    """
    Get the list of Meeting Points we handle.
    """

    return get_all_meeting_points()


@router.get(
    "/availableTimeSlots",
    responses={
        200: {
            "description": "Available time slots successfully found",
            "content": {
                "application/json": {
                    "example": {
                        "506": [
                            {
                                "datetime": "2022-12-19T10:00Z",
                                "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T10:00Z",
                            },
                            {
                                "datetime": "2022-12-19T10:20Z",
                                "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T10:20Z",
                            },
                            {
                                "datetime": "2022-12-19T10:40Z",
                                "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T10:40Z",
                            },
                            {
                                "datetime": "2022-12-19T11:00Z",
                                "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T11:00Z",
                            },
                        ],
                        "7789": [
                            {
                                "datetime": "2022-12-19T10:00Z",
                                "callback_url": "http://www.lille.fr/Lille-Sud2/Mairie-de-quartier-de-Lille-Sud/rendez-vous/passeports?date=2022-12-19T10:00Z",
                            },
                            {
                                "datetime": "2022-12-19T10:20Z",
                                "callback_url": "http://www.lille.fr/Lille-Sud2/Mairie-de-quartier-de-Lille-Sud/rendez-vous/passeports?date=2022-12-19T10:20Z",
                            },
                            {
                                "datetime": "2022-12-19T10:40Z",
                                "callback_url": "http://www.lille.fr/Lille-Sud2/Mairie-de-quartier-de-Lille-Sud/rendez-vous/passeports?date=2022-12-19T10:40Z",
                            },
                            {
                                "datetime": "2022-12-19T11:00Z",
                                "callback_url": "http://www.lille.fr/Lille-Sud2/Mairie-de-quartier-de-Lille-Sud/rendez-vous/passeports?date=2022-12-19T11:00Z",
                            },
                        ],
                    }
                }
            },
        }
    },
    dependencies=[Depends(verify_auth_token)],
)
def get_available_time_slots(
    meeting_point_ids: list[str] = Query(default=Required, example=["201", "203"]),
    start_date: date = Query(default=Required, example="2022-11-01"),
    end_date: date = Query(default=Required, example="2022-11-30"),
):
    """
    Search available time slots.
    """

    all_points = get_all_meeting_points()

    result = {}

    for meeting_point_id in meeting_point_ids:
        meeting_point = None
        index = 0
        while index < len(all_points):
            if meeting_point_id == all_points[index]["id"]:
                meeting_point = all_points[index]
                break
            index += 1

        if not meeting_point:
            raise HTTPException(
                status_code=404, detail=f"Unknown Meeting Point ID : {meeting_point_id}"
            )

        if os.environ.get("MOCK_EDITORS") in ["True", True]:
            available_slots = get_mock_slots(meeting_point, start_date, end_date)
        else:
            available_slots = []

        result[meeting_point_id] = available_slots

    return result


@router.get(
    "/searchApplicationIds",
    responses={
        200: {
            "description": "Application IDs searched successfully!",
            "content": {
                "application/json": {
                    "example": {
                        "6123155111": [
                            {
                                "meeting_point": "Mairie ANNEXE LILLE-SECLIN",
                                "datetime": "2022-12-19T10:00Z",
                                "management_url": "http://www.ville-seclin.fr/rendez-vous/predemande?num=6123155111",
                                "cancel_url": "http://www.ville-seclin.fr/rendez-vous/annulation?num=6123155111",
                            }
                        ]
                    }
                }
            },
        }
    },
    dependencies=[Depends(verify_auth_token)],
)
async def search_application_ids(
    application_ids: list[str] = Query(
        default=Required, example=["6123155111", "6123155222"]
    ),
) -> Any:
    """
    Get the list of meetings by application_id.
    """

    meetings = []

    editor_futures = []
    for editor in get_all_editors():
        editor_futures.append(
            asyncio.ensure_future(editor.search_meetings(application_ids))
        )
    meetings = await asyncio.gather(*editor_futures)

    result = {}
    for editor_meetings in meetings:
        for key in editor_meetings:
            if editor_meetings[key]:
                if key not in result:
                    result[key] = []
                result[key] += editor_meetings[key]

    return result
