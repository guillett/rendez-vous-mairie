import asyncio
import json
import logging
import os
import random

import requests
from fastapi import WebSocket
from pydantic import ValidationError
from src.models.municipality import Municipality
from src.services.mock_data import get_mock_managed_meeting_points, get_mock_slots

_logger = logging.getLogger("__name__")


class Editor:
    slug: str
    name: str
    api_url: str
    _test_mode: bool

    def __init__(self, slug: str, name: str, api_url: str, test_mode: bool):
        self.slug = slug
        self.name = name
        self.api_url = api_url
        self._test_mode = test_mode

    def get_managed_meeting_points(self):
        points = None
        if self._test_mode:
            points = get_mock_managed_meeting_points(self)
        else:
            try:
                headers = {
                    "x-hub-rdv-auth-token": os.environ.get(f"{self.slug}_auth_token")
                }
                response = requests.get(
                    f"{self.api_url}/getManagedMeetingPoints", headers=headers
                )
                if response.status_code in [200]:
                    points = response.json()
            except Exception as get_meeting_points_e:
                print(
                    "Error while getting meeting points for "
                    + self.name
                    + " : "
                    + str(get_meeting_points_e)
                )

        valid_meeting_points = []
        for point in points:
            point["_editor_name"] = self.name
            point["_internal_id"] = str(point["id"])
            try:
                Municipality.parse_obj(point)
                valid_meeting_points.append(point)
            except ValidationError as meeting_point_validation_e:
                _logger.error(
                    "Error while validating meeting point : %s \nError: %s",
                    point,
                    meeting_point_validation_e,
                )

        return valid_meeting_points

    async def get_available_time_slots(self, meeting_points, start_date, end_date):
        result = {}
        if self._test_mode:
            await asyncio.sleep(random.randint(3, 12))
            for meeting_point in meeting_points:
                meeting_point_slots = get_mock_slots(
                    meeting_point, start_date, end_date
                )
                result[meeting_point["_internal_id"]] = meeting_point_slots
        else:
            # this sleep is necessary to not block other async operations
            await asyncio.sleep(0.0000001)
            meeting_point_ids = [x["_internal_id"] for x in meeting_points]
            try:
                headers = {
                    "x-hub-rdv-auth-token": os.environ.get(f"{self.slug}_auth_token")
                }
                parameters = {
                    "start_date": start_date,
                    "end_date": end_date,
                    "meeting_point_ids": meeting_point_ids,
                }
                response = requests.get(
                    f"{self.api_url}/availableTimeSlots",
                    headers=headers,
                    params=parameters,
                )
                if response.status_code in [200]:
                    result = response.json()
            except Exception as get_meeting_points_e:
                print(
                    "Error while getting meeting points for "
                    + self.name
                    + " : "
                    + str(get_meeting_points_e)
                )

        return result

    async def search_slots_in_editor(
        self, meeting_points, start_date, end_date, websocket: WebSocket = None
    ):
        editor_meeting_points = []
        editor_meeting_points_with_slots = []
        for meeting_point in meeting_points:
            if meeting_point["_editor_name"] == self.name:
                editor_meeting_points.append(meeting_point)
        if editor_meeting_points:
            slots = await self.get_available_time_slots(
                editor_meeting_points, start_date, end_date
            )
            for meeting_point in editor_meeting_points:
                if (
                    meeting_point["_internal_id"] in slots
                    and slots[meeting_point["_internal_id"]]
                ):
                    meeting_point["available_slots"] = slots[
                        meeting_point["_internal_id"]
                    ]
                    editor_meeting_points_with_slots.append(meeting_point)
            if websocket:
                json_string = json.dumps(editor_meeting_points_with_slots, default=str)
                await websocket.send_text(json_string)
        return editor_meeting_points_with_slots

    async def search_meetings(self, application_ids):
        # print('Search Application Ids in ' + self.name)
        await asyncio.sleep(0.00001)
        meetings = {}
        if not self._test_mode:
            try:
                headers = {
                    "x-hub-rdv-auth-token": os.environ.get(f"{self.slug}_auth_token")
                }
                parameters = {"application_ids": application_ids}
                response = requests.get(
                    f"{self.api_url}/searchApplicationIds",
                    headers=headers,
                    params=parameters,
                    timeout=5,
                )
                if response.status_code in [200]:
                    meetings = response.json()
            except Exception as search_meetings_e:
                print(
                    "Error while seachring meetings by application ID for "
                    + self.name
                    + " : "
                    + str(search_meetings_e)
                )
        else:
            await asyncio.sleep(random.randint(3, 5))
        # print('Done Searching Application Ids in ' + self.name)
        return meetings


def init_all_editors():
    citopia_editor = Editor(
        "citopia", "Citopia", "https://pro.rendezvousonline.fr/api", False
    )
    synbird_editor = Editor(
        "synbird", "Synbird", "https://sync.synbird.com/ants", False
    )
    rdv360_editor = Editor("rdv360", "RDV360", "https://www.rdv360.com", True)
    orionRDV_editor = Editor("orionRDV", "OrionRDV", "https://orionrdv.com/", True)
    if os.environ.get("MOCK_EDITORS") in ["True", True]:
        return [citopia_editor, rdv360_editor, orionRDV_editor, synbird_editor]
    else:
        return [citopia_editor, synbird_editor]
