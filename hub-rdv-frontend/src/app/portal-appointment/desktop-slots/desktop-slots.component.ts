import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Slot } from '@shared/classes/slot';
import { DeviceStateService } from '@shared/services/device-state.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'rdv-desktop-slots',
  templateUrl: './desktop-slots.component.html',
  styleUrls: ['./desktop-slots.component.scss'],
})
export class DesktopSlotsComponent implements OnInit, OnChanges {
  @Input() slots?: Map<Date, Array<Slot>>;
  @Input() search_in_progress?: boolean;

  slotsRange: any;
  start: number = 0;
  end: number = 7;
  step: number = 7;
  timerSubscription: any;
  lean = true;
  results_length: any;

  constructor(private http: HttpClient, private deviceState: DeviceStateService) {
    this.onScreenChange();
  }

  ngOnInit(): void {}

  ngOnChanges() {
    this.deviceState.resize().subscribe((evt) => {
      this.onScreenChange();
    });
  }

  keys(map: Map<any, any> | undefined) {
    if (map !== undefined) {
      let keys = [...map.keys()];
      return this.sortDates(keys).slice(this.start, this.end);
    }
    return undefined;
  }

  sortDates(list: Array<Date>) {
    return list.sort((a, b) => {
      return <any>new Date(a) - <any>new Date(b);
    });
  }

  select(tabIndex: number) {}

  next() {
    if (this.slots) {
      const lastIndex = this.getLastElementIndex();
      if (lastIndex && this.end + this.step <= lastIndex) {
        this.start += this.step;
        this.end += this.step;
        //this.ngOnInit();
      }
    }
  }

  previous() {
    if (this.start - this.step >= 0) {
      this.start -= this.step;
      this.end -= this.step;
      //this.ngOnInit();
    }
  }

  getLastElementIndex(): number {
    if (this.slots) {
      const rest: number = this.slots.size % this.step;
      if (rest !== 0) {
        return this.slots.size + this.step - rest;
      } else {
        return this.slots.size;
      }
    }
    return 0;
  }

  onScreenChange() {
    const screen = this.deviceState.getScreen();
    switch (screen) {
      case 'MD':
        this.step = 5;
        this.end = 5;
        break;
      case 'LG':
      case 'XL':
        this.step = 7;
        this.end = 7;
        break;
      default:
        this.step = 3;
        this.end = 3;
        break;
    }
  }
}
