/* Defines the product entity */
export interface SlotsFromPosition {
  id: number;
  name: string;
  longitude: number;
  latitude: number;
  editor_id: string;
  public_entry_address: string;
  zip_code: string;
  city_name: string;
  website: string;
  city_logo: string;
  available_slots: any[];
}
