import { Component, OnInit } from '@angular/core';
import { DeviceStateService } from '@shared/services/device-state.service';

@Component({
  selector: 'rdv-portal-appointment',
  templateUrl: './portal-appointment.component.html',
  styleUrls: ['./portal-appointment.component.scss'],
})
export class PortalAppointmentComponent implements OnInit {
  desktop: boolean = true;

  constructor(private deviceState: DeviceStateService) {
    this.desktop = !deviceState.isMobileResolution();
  }

  ngOnInit(): void {}
}
