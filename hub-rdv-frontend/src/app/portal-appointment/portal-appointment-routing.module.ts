import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Import the 3 new components/sections of the webpage so that we can configure routing
import { ContactComponent } from './contact/contact.component';
import { CGUMentionLegalComponent } from './CGU-MentionLegal/CGUMentionLegal.component';
import { PredemandeComponent } from './pre-demande/pre-demande.component';
// Declare the paths that will be used when navigating between pages.
const routes: Routes = [
  { path: 'contact', component: ContactComponent },
  { path: 'CGU-mentions-legales', component: CGUMentionLegalComponent },
  { path: 'NumeroPredemande', component: PredemandeComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class PortalAppointmentRoutingModule {}
