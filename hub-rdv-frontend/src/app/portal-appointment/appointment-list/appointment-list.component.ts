import { Component, Input, OnInit } from '@angular/core';
import { Slot } from '@shared/classes/slot';

@Component({
  selector: 'rdv-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss'],
})
export class AppointmentListComponent implements OnInit {
  @Input() index: number = -1;
  @Input() slotsByDay?: Array<Slot>;
  @Input() day?: Date;

  timeZone: any;
  local: any;

  constructor() {
    this.timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this.local = Intl.DateTimeFormat().resolvedOptions().locale;
    this.day?.toLocaleDateString(this.local);
  }

  ngOnInit(): void {}

  __getLocaleTimeFormat__(d: any) {
    let date = new Date(d);
    var minutes = ('0' + (date.getMinutes() + 1)).slice(-2);
    let appointement = date.getHours() + ':' + minutes;
    return appointement;
  }
}
