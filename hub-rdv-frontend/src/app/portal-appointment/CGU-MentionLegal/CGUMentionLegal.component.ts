/* Use typescript for the behaviour/backend work here */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rdv-contact',
  templateUrl: './CGUMentionLegal.component.html',
  styleUrls: ['./CGUMentionLegal.component.css'],
})
export class CGUMentionLegalComponent implements OnInit {
  post = ''; // add this
  href = ''; // add this

  constructor(private route: ActivatedRoute) {} // Modify this, to add the ActivatedRoute

  ngOnInit(): void {
    this.href = window.location.href; // add this
    this.post = 'assets/CGU-mentions-légales.md'; // add this
  }
}
