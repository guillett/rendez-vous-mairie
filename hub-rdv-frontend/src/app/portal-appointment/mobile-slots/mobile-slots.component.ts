import { Component, Input, OnInit } from '@angular/core';
import { Slot } from '@shared/classes/slot';

@Component({
  selector: 'rdv-mobile-slots',
  templateUrl: './mobile-slots.component.html',
  styleUrls: ['./mobile-slots.component.scss'],
})
export class MobileSlotsComponent implements OnInit {
  @Input() slots?: Map<Date, Array<Slot>>;
  @Input() search_in_progress?: boolean;
  slotsRange: any;

  constructor() {}

  ngOnInit(): void {
    const array = new Array();
    array.push(this.getSlotMock());
    array.push(this.getSlotMock());
    this.slots = new Map();
    let date = new Date();
    this.slots.set(date, array);
    date = new Date();
    date.setDate(date.getDate() + 1);
    this.slots.set(date, array);
    date = new Date();
    date.setDate(date.getDate() + 2);
    this.slots.set(date, array);
    date = new Date();
    date.setDate(date.getDate() + 3);
    this.slots.set(date, array);
    date = new Date();
    date.setDate(date.getDate() + 4);
    this.slots.set(date, array);
    date = new Date();
    date.setDate(date.getDate() + 5);
    this.slots.set(date, array);
    date = new Date();
    date.setDate(date.getDate() + 6);
    this.slots.set(date, array);
  }

  keys(map: Map<any, any> | undefined) {
    if (map !== undefined) {
      let keys = [...map.keys()];
      return this.sortDates(keys);
    }
    return undefined;
  }

  sortDates(list: Array<Date>) {
    return list.sort((a, b) => {
      return <any>new Date(a) - <any>new Date(b);
    });
  }

  getSlotMock() {
    const slot: Slot = new Slot();
    slot.id = 12453;
    slot.municipalityId = 3;
    slot.municipality = 'Sartrouville';
    slot.duration = 15;
    slot.distance = 2;
    slot.url = 'https://www.doctolib.fr/';
    slot.hour = '08:15';
    return slot;
  }
}
