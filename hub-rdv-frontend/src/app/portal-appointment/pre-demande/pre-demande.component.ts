/* Use typescript for the behaviour/backend work here */
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, ValidationErrors } from '@angular/forms';
@Component({
  selector: 'rdv-predemande',
  templateUrl: './pre-demande.component.html',
  styleUrls: ['./pre-demande.component.scss'],
})
export class PredemandeComponent implements OnInit {
  predemande_results: any;
  showresults: boolean = false;
  public predemandes: string = '';
  form: FormGroup;
  ValidateError: boolean = false;
  clickedSearch: boolean = false;

  /*
  form = new FormGroup({
    name: new FormControl(),
  });
  */

  RegexFormat = /^([a-zA-Z0-9]{10}[,;:\-/.\s])*[a-zA-Z0-9]{0,10}$/;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private route: ActivatedRoute) {
    this.form = this.formBuilder.group({
      predemandes: [null, [this.RegexValidator()]],
    });
  }

  ngOnInit(): void {}

  clickSearch() {
    this.clickedSearch = true;
    let predemandes = window.document.getElementById('predemandes')! as HTMLInputElement;
    if (predemandes.value.match(/^([a-zA-Z0-9]{10}[,;:\-/.\s])*[a-zA-Z0-9]{10}$/)) {
      this.showresults = true;
      const headers = new HttpHeaders()
        .set('content-type', 'application/json')
        .set('Access-Control-Allow-Origin', '*')
        .set('x-hub-rdv-auth-token', 'test-token');
      this.predemande_results = new Array();
      const application_ids = this.form.get('predemandes')?.value.split(/[,;:\s]/);
      let url = 'https://rendez-vous-api.france-identite.fr/api/searchApplicationIds?';
      application_ids.forEach((Element: any) => (url = url + 'application_ids=' + Element + '&'));
      url = url.slice(0, -1);
      this.http
        .get(url, {
          headers: headers,
        })
        .subscribe((res: any) => {
          Object.keys(res).forEach((key, index) => {
            res[key].forEach((row: any) => {
              row['id'] = key;
              this.predemande_results.push(row);
            });
          });
        });
      console.log(this.predemande_results);
    } else this.ValidateError = true;
  }

  __getLocaleTimeFormat__(d: any) {
    let date = new Date(d);
    var minutes = ('0' + (date.getMinutes() + 1)).slice(-2);
    let appointement = date.getHours() + ':' + minutes;
    return appointement;
  }

  RegexValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value && !control.value.toString().match(this.RegexFormat)) {
        this.ValidateError = true;
        return { valide: true };
      }
      this.ValidateError = false;
      return null;
    };
  }
}
