/* Use typescript for the behaviour/backend work here */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rdv-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {
  post = ''; // add this
  href = ''; // add this

  constructor(private route: ActivatedRoute) {} // Modify this, to add the ActivatedRoute

  ngOnInit(): void {
    this.href = window.location.href; // add this
    this.post = 'assets/contact.md'; // add this
  }
}
