import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DeviceStateService } from '@shared/services/device-state.service';
import * as moment from 'moment';
import { Slot } from '@shared/classes/slot';
import { WebsocketService } from '@shared/services/websocket.service';
import { NgProgressComponent } from 'ngx-progressbar';
import { ViewportScroller } from '@angular/common';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'rdv-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  name = 'Angular';
  lat: any;
  lng: any;
  clickMessage: any = '';
  empty_location: boolean = true;

  showresults: boolean = false;
  @ViewChild(NgProgressComponent) progressBar?: NgProgressComponent;
  desktop: boolean = true;

  public nameForm: FormGroup;
  start_date: any;
  end_date: any;
  radius_km: string = '40';

  rawSlots: Array<any> = [];
  slots?: Map<Date, Array<Slot>>;

  search_in_progress: boolean = true;
  scrolled: boolean = false;

  token: string | undefined;

  search_clicked: boolean = false;

  recaptcha_clicked: boolean = false;

  constructor(
    private deviceState: DeviceStateService,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private websocketService: WebsocketService,
    private scroller: ViewportScroller,
    private recaptchaV3Service: ReCaptchaV3Service
  ) {
    this.desktop = !deviceState.isMobileResolution();
    this.token = undefined;

    let startMoment: moment.Moment = moment(new Date());
    this.start_date = startMoment.format('YYYY-MM-DD');
    let endMoment: moment.Moment = moment(startMoment).add(3, 'M');
    this.end_date = endMoment.format('YYYY-MM-DD');

    this.nameForm = this.formBuilder.group({
      start_date: this.start_date,
      end_date: this.end_date,
      radius_km: '20',
      recaptcha: [null, Validators.required],
    });

    this.websocketService.messages.subscribe((data) => {
      if (data != 'end_of_search') {
        this.rawSlots = this.rawSlots.concat(JSON.parse(data));
        this.sortSlots();
      } else {
        this.search_in_progress = false;
        this.scroll();
        this.progressBar?.complete();
      }
    });
  }
  scroll() {
    if (!this.scrolled) {
      if (this.slots?.size !== 0 || !this.search_in_progress) {
        let scrolltoelement = window.document.getElementById('target')! as HTMLInputElement;
        if (scrolltoelement) {
          scrolltoelement.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
            inline: 'nearest',
          });
          this.scrolled = true;
        }
      } else
        setTimeout(() => {
          this.scroll();
        }, 500);
    }
  }

  onSubmit() {
    console.log(this.nameForm.controls);
  }

  handleSuccess(e: any) {
    console.log('ReCaptcha', e);
  }

  clickSearch() {
    this.search_clicked = true;

    let name = window.document.getElementById('name')! as HTMLInputElement;
    let email = window.document.getElementById('email')! as HTMLInputElement;

    if (name.value == '' && email.value == '') {
      const location = window.document.getElementById('location')! as HTMLInputElement;
      location.value = this.clickMessage;

      if (location.value == '') this.empty_location = true;

      this.recaptchaV3Service.execute('importantAction').subscribe((token: string) => {
        console.debug(`Token [${token}] generated`);

        this.empty_location = false;
        this.search_in_progress = true;
        this.scrolled = false;

        if (!this.showresults) this.showresults = !this.showresults;
        this.progressBar?.start();
        this.start_date = this.nameForm.get('start_date')?.value;
        this.end_date = this.nameForm.get('end_date')?.value;
        this.radius_km = this.nameForm.get('radius_km')?.value;
        this.rawSlots = [];
        this.sortSlots();
        let message = {
          latitude: this.lat,
          longitude: this.lng,
          start_date: this.start_date,
          end_date: this.end_date,
          radius_km: this.radius_km,
        };
        // Websocket
        this.websocketService.messages.next(message);

        // HTTP request
        // this.http
        //   .get(
        //     'https://rendez-vous-api.france-identite.fr/api/SlotsFromPosition?longitude=50.612875943839&latitude=3.0475818403133&start_date=2022-11-01&end_date=2022-11-30&radius_km=15'
        //   )
        //   .subscribe((res: any) => {
        //     this.rawSlots = res;
        //     this.sortSlots();
        //   });
        setTimeout(() => {
          this.scroll();
        }, 2000);
      });
    }
  }

  sortSlots() {
    this.slots = new Map();
    let slotsperdate = new Map();
    const array = new Array();

    this.rawSlots.forEach((mairie: any) => {
      mairie['available_slots'].forEach((s: any) => {
        let d = new Date(s['datetime']);
        var minutes = ('0' + (d.getMinutes() + 1)).slice(-2);
        let appointement = d.getHours() + ':' + minutes;
        const day = d.getDate();
        const month = d.getMonth() + 1; // getMonth() returns month from 0 to 11
        const year = d.getFullYear();
        const str = `${year}-${month}-${day}`;
        const slot: Slot = new Slot();
        slot.municipality = mairie['name'];
        slot.distance = mairie['distance_km'].toFixed(1);
        slot.url = s.callback_url;
        //slot.hour = appointement;
        slot.hour = d.toString();
        slot.logo = mairie['city_logo'];
        slotsperdate?.set(str, new Array());
        array.push(slot);
      });
    });

    for (let i = 0; i < array.length; i++) {
      let slot: Slot = array[i];
      if (slot.hour) {
        let d = new Date(slot.hour);
        const day = d.getDate();
        const month = d.getMonth() + 1; // getMonth() returns month from 0 to 11
        const year = d.getFullYear();
        const str = `${year}-${month}-${day}`;
        slotsperdate.get(str)?.push(slot);
      }
    }

    for (const [key, value] of slotsperdate.entries()) {
      this.slots.set(
        new Date(key.toString()),
        value.sort(function (a: any, b: any) {
          return <any>new Date(a._hour ? a._hour : '') - <any>new Date(b._hour ? b._hour : '');
        })
      );
    }
  }

  ngOnInit() {
    let start_end_date: any = window.document.getElementById('start-date')! as HTMLInputElement;
    start_end_date.value = this.start_date;
    start_end_date.min = this.start_date;
    start_end_date = window.document.getElementById('end-date')! as HTMLInputElement;
    start_end_date.value = this.end_date;
  }

  getUserLocation() {
    // get Users current position
    this.progressBar?.start();
    this.empty_location = false;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.http
          .get('https://api-adresse.data.gouv.fr/reverse/?lon=' + this.lng.toString() + '&lat=' + this.lat.toString() + '&type=street')
          .subscribe((res: any) => {
            this.clickMessage = res.features[0].properties.label;
            const location = window.document.getElementById('location')! as HTMLInputElement;
            location.value = this.clickMessage;
            this.progressBar?.complete();
          });
      });
    } else {
      console.log('User not allowed');
    }
  }

  public send(form: any): void {
    this.search_clicked = true;

    if (!this.empty_location) {
      this.recaptchaV3Service.execute('myAction').subscribe({
        next: (token) => {
          //console.log('Recaptcha v3 token', token);
          this.empty_location = false;
          this.search_in_progress = true;
          this.scrolled = false;
          this.search_clicked = true;
          if (!this.showresults) this.showresults = !this.showresults;
          this.progressBar?.start();
          this.start_date = this.nameForm.get('start_date')?.value;
          this.end_date = this.nameForm.get('end_date')?.value;
          this.radius_km = this.nameForm.get('radius_km')?.value;
          this.rawSlots = [];
          this.sortSlots();
          let message = {
            latitude: this.lat,
            longitude: this.lng,
            start_date: this.start_date,
            end_date: this.end_date,
            radius_km: this.radius_km,
          };
          // Websocket
          this.websocketService.messages.next(message);
          this.scroll();
        },
        error: (e) => console.log(`Recaptcha v3 error:`, e),
      });
    }
  }
}
