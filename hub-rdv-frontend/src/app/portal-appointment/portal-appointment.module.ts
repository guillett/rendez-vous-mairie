import { CommonModule, registerLocaleData } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { PortalAppointmentComponent } from './portal-appointment/portal-appointment.component';
import { DesktopSlotsComponent } from './desktop-slots/desktop-slots.component';
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { SharedModule } from '../shared/shared.module';
import { MobileSlotsComponent } from './mobile-slots/mobile-slots.component';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PortalAppointmentRoutingModule } from './portal-appointment-routing.module';
import { ContactComponent } from './contact/contact.component';
import { CGUMentionLegalComponent } from './CGU-MentionLegal/CGUMentionLegal.component';
import { PredemandeComponent } from './pre-demande/pre-demande.component';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule, SecurityContext } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgProgressModule } from 'ngx-progressbar';
import { NgProgressHttpModule } from 'ngx-progressbar/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  RecaptchaModule,
  RECAPTCHA_SETTINGS,
  RecaptchaSettings,
  RecaptchaFormsModule,
  RECAPTCHA_V3_SITE_KEY,
  RecaptchaV3Module,
} from 'ng-recaptcha';
import { environment } from '../../environments/environment';
import { ReCaptchaV3Service } from 'ng-recaptcha';

registerLocaleData(localeFr);

const RECAPTCHA_V3_KEY = '6Leu1eYhAAAAANv8x5qG9sQnbiNmyX2w03GCutK7';

@NgModule({
  declarations: [
    SearchComponent,
    PortalAppointmentComponent,
    DesktopSlotsComponent,
    AppointmentListComponent,
    MobileSlotsComponent,
    ContactComponent,
    CGUMentionLegalComponent,
    PredemandeComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PortalAppointmentRoutingModule,
    HttpClientModule, // add this,
    NgProgressModule.withConfig({ color: '#000091' }),
    NgProgressHttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MarkdownModule.forRoot({ loader: HttpClient, sanitize: SecurityContext.NONE }), // add this
    RecaptchaFormsModule,
    RecaptchaModule,
    RecaptchaV3Module,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    ReCaptchaV3Service,
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: RECAPTCHA_V3_KEY,
    },
  ],
})
export class PortalAppointmentModule {}
