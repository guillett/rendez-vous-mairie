import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DeviceStateService {
  private mobileResolution: boolean;

  constructor() {
    if (window.innerWidth < 768) {
      this.mobileResolution = true;
    } else {
      this.mobileResolution = false;
    }
  }

  public isMobileResolution(): boolean {
    return this.mobileResolution;
  }

  public getScreen(): string {
    const width: number = window.innerWidth;
    let screen: string = 'LG';
    if (width < 576) {
      screen = 'XS';
    } else if (width >= 576 && width < 768) {
      screen = 'SM';
    } else if (width >= 768 && width < 992) {
      screen = 'MD';
    } else if (width >= 992 && width < 1248) {
      screen = 'LG';
    } else {
      screen = 'XL';
    }
    return screen;
  }

  public resize(): Observable<Event> {
    return fromEvent(window, 'resize');
  }
}
