import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

const CHAT_URL = 'wss://rendez-vous-api.france-identite.fr/api/SlotsFromPositionStreaming';

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  private subject?: AnonymousSubject<MessageEvent>;
  public messages: Subject<any>;

  constructor() {
    this.messages = <AnonymousSubject<any>>this.connect(CHAT_URL).pipe(
      map((response: MessageEvent): any => {
        return response.data;
      })
    );
  }

  public connect(url: string): AnonymousSubject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
      console.log('Successfully connected: ' + url);
    }
    return this.subject;
  }

  private create(url: string): AnonymousSubject<MessageEvent> {
    let ws = new WebSocket(url);
    let observable = new Observable((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    let observer = {
      error: (err: any) => {
        console.error('Websocket error: ' + err);
      },
      complete: () => {
        console.error('Websocket complete!');
      },
      next: (data: Object) => {
        console.log('Message sent to websocket: ', data);
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        } else {
          console.error('Websocket needs to reconnect!');
        }
      },
    };
    return new AnonymousSubject<MessageEvent>(observer, observable);
  }
}
