import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

const shared = [HeaderComponent, FooterComponent];

@NgModule({
  declarations: [...shared],
  imports: [CommonModule, RouterModule],
  exports: [...shared],
})
export class SharedModule {}
