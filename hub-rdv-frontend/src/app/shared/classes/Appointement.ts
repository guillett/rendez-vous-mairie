export class Appointement {
  _id?: number;
  // change to municipality Object
  _municipalityId?: number;
  _date?: Date;
  _duration? = null;
  _logo?: string;
  constructor() {}

  get id(): number | undefined {
    return this._id;
  }
  set id(id: number | undefined) {
    this._id = id;
  }

  get municipalityId() {
    return this.municipalityId;
  }
  set municipalityId(municipalityId: number) {
    this.municipalityId = municipalityId;
  }

  get date(): Date | undefined {
    return this._date;
  }
  set date(date: Date | undefined) {
    this._date = date;
  }

  get duration() {
    return this._duration;
  }
  set duration(duration) {
    this._duration = duration;
  }
  get logo(): string | undefined {
    return this._logo;
  }
  set logo(_logo: string | undefined) {
    this._logo = _logo;
  }
}
