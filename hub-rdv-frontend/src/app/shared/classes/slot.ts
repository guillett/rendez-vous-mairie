export class Slot {
  _id?: number;
  // change to municipality Object
  _municipalityId?: number;
  _municipality?: string;
  _date?: Date;
  _duration: number = 0;
  _distance: number = 0;
  _url?: string;
  _hour?: string;
  _logo?: string;

  constructor() {}

  get id(): number | undefined {
    return this._id;
  }
  set id(id: number | undefined) {
    this._id = id;
  }

  get municipalityId(): number | undefined {
    return this._municipalityId;
  }
  set municipalityId(municipalityId: number | undefined) {
    this._municipalityId = municipalityId;
  }

  get municipality() {
    return this._municipality;
  }
  set municipality(municipality) {
    this._municipality = municipality;
  }

  get date(): Date | undefined {
    return this._date;
  }
  set date(date: Date | undefined) {
    this._date = date;
  }

  get duration() {
    return this._duration;
  }
  set duration(duration) {
    this._duration = duration;
  }

  get distance() {
    return this._distance;
  }
  set distance(distance) {
    this._distance = distance;
  }

  get url() {
    return this._url;
  }
  set url(url) {
    this._url = url;
  }

  get hour() {
    return this._hour;
  }
  set hour(hour) {
    this._hour = hour;
  }
  get logo(): string | undefined {
    return this._logo;
  }
  set logo(_logo: string | undefined) {
    this._logo = _logo;
  }
}
