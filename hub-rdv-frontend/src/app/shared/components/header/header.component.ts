import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'rdv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  appName = 'Agence nationale des titres sécurisés';
  appDescription = "Moteur de recherche de rendez vous pour la délivrance des passeports et de carte d'identité";
  routes: Map<string, string> = new Map([
    ['/NumeroPredemande', 'Recherchez un rendez-vous'],
    ['', 'Annulez vos rendez-vous en double'],
    ['/contact', 'Recherchez un rendez-vous'],
    ['/CGU-mentions-legales', 'Recherchez un rendez-vous'],
  ]);
  names: Map<string, string> = new Map([
    ['/NumeroPredemande', ''],
    ['/contact', ''],
    ['/CGU-mentions-legales', ''],
    ['', '/NumeroPredemande'],
  ]);

  constructor(private location: Location) {}
  ngOnInit(): void {
    console.log(this.location.path());
    console.log(window.location.href);
  }

  getCurrentPath() {
    return { path: this.names.get(this.location.path()), name: this.routes.get(this.location.path()) };
  }
}
