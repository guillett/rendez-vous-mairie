import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PortalAppointmentComponent } from './portal-appointment/portal-appointment/portal-appointment.component';

const routes: Routes = [{ path: '', component: PortalAppointmentComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
