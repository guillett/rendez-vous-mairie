# Mentions légales
___

## Informations éditeurs

Le site internet de l'Agence nationale des titres sécurisés (ANTS) est édité et géré par l'ANTS.
**18 rue Irénée Carré**
**BP 70474**
**08101 Charleville-Mézières**

## **Directeur de la publication**

Mme BAUDOUIN – Directrice de l'ANTS et directrice de la publication

## Hébergement

L'hébergement et les développements du site _rendez-vous.france-identite.fr_ est assuré par la société Scalingo :

**Scalingo**
**13 RUE JACQUES PEIROTES, 67000 STRASBOURG,**

[**https://www.scalingo.com**](https://www.scalingo.com/)


## Traitement des données personnelles

Aucune donnée personnelle n'est utilisée sur le présent site.

## Droit d'auteur

### **Créations graphiques - Crédits photographiques - Vidéos**

Pour en savoir plus sur les règles du code de la propriété intellectuelle régissant le portail, cliquez ici : [**Chapitre 1.9.1 CGU**](https://passeport.ants.gouv.fr/conditions-generales-d-utilisation-cgu)

### **La mise en place de lien vers les sites**

Pour en savoir plus sur les conditions de mise en place des liens vers nos sites, cliquez ici : [**Chapitre 1.9.2 CGU**](https://passeport.ants.gouv.fr/conditions-generales-d-utilisation-cgu)

### **Reprise du contenu mis en ligne**

Pour en savoir plus sur les conditions de mise en place des liens vers nos sites, cliquez ici : [**Chapitre 1.9.2 CGU**](https://passeport.ants.gouv.fr/conditions-generales-d-utilisation-cgu)

### **La marque ANTS**

Pour en savoir plus sur la marque ANTS, cliquez ici : [**Chapitre 1.9.4 CGU**](https://passeport.ants.gouv.fr/conditions-generales-d-utilisation-cgu)

### **Liens vers des sites tiers**

Pour en savoir plus sur les liens vers les sites tiers proposés sur les sites de l'ANTS, cliquez ici : [**Chapitre 1.9.3 CGU**](https://passeport.ants.gouv.fr/conditions-generales-d-utilisation-cgu)

## Politique de confidentialité - Utilisation de cookies

L'utilisation de ce site ne requiert pas l'utilisation de cookies.
<br>
<br>
<br>
# Conditions Générales d'Utilisation (CGU)
___

## 1.1    Objet

L'utilisation de ce site est gratuite, l'objectif est de permettre aux utilisateurs de chercher un rendez-vous près de chez eux en vue d'obtenir une carte d'identité ou un passeport.

Les termes des présentes CGU peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées au Portail, de l'évolution de la législation ou pour tout autre motif jugé nécessaire.

Les modifications entrent alors en vigueur à compter de leur publication sur le Portail, le cas échéant, à compter de toute autre date qui serait indiquée. Tout utilisateur est réputé avoir pris connaissance de la nouvelle version des CGU, dans le portail du simple fait de l'accès au portail.

Il est de la responsabilité de l'utilisateur du Portail de consulter les CGU régulièrement.

## 1.2    Utilisation de la géolocalisation

Le site permet de géolocaliser les lieux et les rendez-vous disponible dans un rayon défini par l'utilisateur. Ces informations sont issues d'un référentiel interne à l'agence et des remontées des éditeurs de plateformes de rendez-vous.

Malgré le soin apporté à la publication de ces données de géolocalisation, l'ANTS ne s'engage pas sur l'exactitude et l'exhaustivité des informations délivrées.

## 1.3    Modalité d'accès

L'ANTS s'engage, dans le cadre d'une obligation de moyens, à assurer la disponibilité et l'accessibilité au portail et ce, 24/24 heures et 7/7 jours. Néanmoins, les opérations de tests, de contrôle et/ou de maintenance ainsi que les interventions nécessaires en cas de panne ou d'alerte de sécurité, peuvent être effectuées par l'ANTS à tout moment. L'ANTS s'efforce de prévenir les utilisateurs, dans la mesure du possible, au moyen d'une annonce sur le portail, lors d'une telle opération. L'ANTS ne saurait être tenue responsable de toute conséquence directe ou indirecte susceptible d'en résulter pour tout utilisateur.

Tout utilisateur reconnaît connaître et comprendre l'Internet, les réseaux de communication électronique et leurs limites et, notamment, leurs caractéristiques fonctionnelles et performances techniques, les risques d'interruption, les temps de réponse pour consulter, interroger ou transférer des informations, les risques, quels qu'ils soient, inhérents à tout transfert de données notamment sur un réseau ouvert.

## 1.4    Sécurité du portail

L'ANTS se réserve la faculté de suspendre l'accès aux sites lorsqu'elle estime qu'un événement susceptible d'en affecter le fonctionnement ou l'intégrité le nécessite ou en cas de maintenance, et ce, pour la durée nécessaire à l'intervention envisagée. Le cas échéant, dès qu'elle aura connaissance des dates d'interventions programmées, l'ANTS s'engage à prévenir les utilisateurs dans les meilleurs délais sur la page d'accueil du portail ou par tout autre procédé à sa convenance, et à réduire la période d'indisponibilité durant la journée. Toutefois, cette suspension ne peut en aucun cas engager la responsabilité de l'ANTS.

## 1.5    Droit de propriété intellectuelle

### 1.5.1     Créations graphiques, crédits photographiques et vidéos sur le portail

Le portail est protégé par la législation française et notamment par le droit de la propriété intellectuelle.

En application de l'article L.122-4 du Code de propriété intellectuelle, « Toute représentation ou reproduction intégrale ou partielle faite sans le consentement de l'auteur [ici, l'ANTS] est illicite (…) », et ce quel que soit le procédé utilisé.

Sont concernés par cette protection la structure générale du site, son contenu, ses logos et visuels, les créations graphiques, les crédits photographiques, les vidéos, les bases de données extraites, copiées ou faites à partir du contenu du portail, par un moyen automatisé ou non.

Par conséquent, toute reproduction intégrale ou partielle de ce site ou l'un des éléments le composant est strictement interdite. L'ANTS se réserve le droit de demander des dommages et intérêts en cas d'atteinte à ses droits de propriété intellectuelle, et se réserve le droit de porter plainte pour contrefaçon en cas d'utilisation ou de reproduction frauduleuse des sites et des éléments les composants.

### 1.5.2 Les marques

L'ANTS constitue une marque déposée et enregistrée auprès de l'Institution National de Propriété Industrielle. Toute utilisation, reproduction, imitation de la marque ANTS de manière à induire tout utilisateur en erreur constitue, à défaut d'autorisation de l'ANTS, une contrefaçon susceptible de poursuites civiles suivant l'article L.716-4 du Code de propriété intellectuelle, et peut donner lieu à une demande de dommages et intérêts.

### 1.5.3 Avertissements

Malgré tout le soin apporté par nos équipes, des erreurs typographiques ou des inexactitudes techniques ne peuvent être exclues. L'ANTS se réserve le droit de les corriger à tout moment dès qu'elles sont portées à sa connaissance.

Les informations sur le site sont ainsi susceptibles de faire l'objet de mises à jour à tout moment.

## 2.1    Responsabilités

### 2.1.1     Responsabilité de l'ANTS

L'ANTS ne consent aucune garantie sur l'aptitude du portail à répondre à des attentes ou besoins particuliers de tout utilisateur.

De la même manière, l'ANTS n'est pas en mesure de garantir qu'aucune erreur ou autre dysfonctionnement n'apparaîtra au cours de l'utilisation du portail.

De même, l'ANTS décline toute responsabilité à l'égard de l'usage non conforme aux présentes CGU qui est fait du portail par tout utilisateur.

La responsabilité de l'ANTS se limite à la vérification des données d'identification personnelle.

En aucun cas, l'ANTS n'est responsable des préjudices tels que notamment : préjudice financier, commercial, perte de clientèle, trouble commercial quelconque, perte de bénéfice, perte d'image de marque, perte de programmes informatiques subis par l'utilisateur qui pourraient résulter de l'inexécution des présentes CGU, lesquels préjudices sont, de convention expresse, réputés avoir le caractère de préjudice indirect.

En outre, l'ANTS n'assume aucun engagement ni responsabilité :

- quant à l'utilisation du portail par l'utilisateur non conforme à la réglementation en vigueur relative à la protection des logiciels ;
- quant à l'usure normale des médias informatiques de l'utilisateur, ou à la détérioration des informations portées sur lesdits médias informatiques due à l'influence des champs magnétiques.

### 2.1.2      Responsabilité de l'utilisateur

L'utilisateur utilise le service sous son entière responsabilité. Il reconnaît que sa responsabilité peut être engagée, notamment dans le cas d'une utilisation frauduleuse du service. L'utilisateur s'engage à utiliser le service conformément aux présentes CGU et à la réglementation en vigueur.

L'utilisateur est responsable de l'utilisation de ses données d'identification personnelle.

L'utilisateur reconnaît que les services proposés aux profils particuliers sont à usage uniquement personnel et que toute utilisation du dit service est effectuée sous sa pleine et entière responsabilité.

Chaque utilisateur s'engage à ne pas modifier, essayer de modifier ou porter atteinte au site internet de quelque manière que ce soit et à ne pas utiliser de logiciel ou toute forme de programme informatique ayant pour but d'atteindre ou de rendre disponible un contenu protégé ou non disponible librement. Il est également interdit de créer une œuvre, une application ou un site dérivant de tout ou partie du présent portail.

L'utilisateur s'engage à informer l'ANTS sans délai, par tout moyen, de toute erreur, faute ou irrégularité qu'il constaterait dans l'utilisation du portail et ce, dès qu'il en a connaissance.

### 2.1.3     Force Majeure

L'ANTS ne saurait être tenue responsable pour tout retard dans l'exécution de ses obligations ou pour toute inexécution de ses obligations résultant des présentes CGU lorsque les circonstances y donnant lieu relèvent de la force majeure, se définissant comme tout évènement imprévisible, irrésistible et extérieur.

De façon expresse, sont considérés notamment comme cas de force majeure ou cas fortuit, outre ceux habituellement retenus par la jurisprudence des cours et tribunaux français, des clauses contractuelles contenues dans les présentes CGU :

Grève totale ou partielle, lock-out, émeute, trouble civil, insurrection, guerre civile ou étrangère, risque nucléaire, embargo, confiscation, capture ou destruction par toute autorité publique, intempérie, épidémie, blocage des moyens de transport ou d'approvisionnement pour quelque raison que ce soit, tremblement de terre, incendie, tempête, inondation, dégâts des eaux, restrictions gouvernementales ou légales, modifications légales ou réglementaires des formes de commercialisation, blocage des communications électroniques, y compris des réseaux de communications électroniques, non prévisibles par l'ANTS, remettant en cause les normes et standards de sa profession et tout autre cas indépendant de la volonté des parties empêchant l'exécution normale des obligations découlant des présentes CGU.

Tout cas de force majeure affectant l'exécution des obligations résultantes des présentes CGU et notamment l'accès ou l'utilisation du service par l'utilisateur suspendra, dès sa date de survenance, l'exécution des présentes CGU. A partir de cette date, et malgré le cas de force majeure, l'ANTS s'efforcera dans la mesure du possible :

- d'informer les utilisateurs de l'existence de ce cas de force majeure ;
- de mettre en œuvre toute autre solution technique permettant aux utilisateurs d'initier leurs demandes d'immatriculation, de permis ou de titres d'identité.

La mise en œuvre tout à fait exceptionnelle de ces moyens palliatifs par l'ANTS pendant la survenance d'un cas de force majeure ne pourra donner lieu à aucune responsabilité ou indemnisation de la part de l'ANTS.

## 2.3    Sous-traitance

L'ANTS se réserve le droit de faire exécuter tout ou partie des prestations objets des présentes CGU par toute société de son choix en sous-traitance, l'ANTS restant seule responsable à l'égard de l'utilisateur, à charge pour elle de se retourner contre ses sous-traitants.

## 2.4    Non-renonciation

Le fait pour l'utilisateur et/ou l'ANTS de ne pas se prévaloir d'un manquement par la partie défaillante à l'une quelconque des obligations résultant des CGU ne saurait être interprété comme une renonciation à l'obligation en cause.

## 2.5     Permanence

La nullité d'une clause quelconque des CGU n'affecte pas la validité des autres clauses ; elles se poursuivent en l'absence du dispositif annulé sauf si la clause annulée rend la poursuite des relations contractuelles impossible ou déséquilibrée par rapport aux relations contractuelles initiales.

## 2.6     Réclamation

Afin d'éviter toute réclamation tardive, et notamment pour permettre à l'ANTS de préserver tous les éléments de preuve, l'utilisateur devra notifier à l'ANTS qu'il entend mettre en œuvre sa responsabilité.

Cette notification devra indiquer de manière précise les erreurs, manquements ou retards constatés et devra être adressée au plus tard dans les trente (30) jours suivant la survenance de l'événement susceptible d'engager la responsabilité de l'ANTS.

Toute réclamation concernant les sites et/ou le service, doit être adressée à l'ANTS :

- par lettre recommandée avec accusé de réception : ANTS, 18 rue Irénée Carré – BP 70474, 08101 Charleville-Mézières.

L'ANTS_ ** ** _s'engage à prendre en compte la réclamation de tout utilisateur dans les soixante (60) jours à compter de sa réception à l'adresse indiquée et de juger des conséquences juridiques à donner à cette réclamation.

## 2.7    Convention de preuve

L'ANTS et l'utilisateur entendent fixer, dans le cadre du service, les règles relatives aux preuves recevables entre eux en cas de litige et à leur force probante. Les stipulations qui suivent constituent ainsi la convention de preuve passée entre les parties, lesquelles s'engagent à respecter le présent article.

Les parties acceptent qu'en cas de litige, les comptes, codes de sécurité confidentiels utilisés dans le cadre du service soient admissibles devant les tribunaux et feront preuve des données et des faits qu'ils contiennent ainsi que les moyens d'identification et procédés d'authentification qu'ils expriment.

Les parties acceptent qu'en cas de litige, les journaux d'événements, les données de connexion relatives à des actions effectuées à partir de leurs comptes sont admissibles devant les tribunaux et font preuve des données et des faits qu'ils contiennent.

## 2.8    Intitule des clauses

Les intitulés portés en tête de chaque article ne servent qu'à la commodité de la lecture et ne peuvent en aucun cas être le prétexte d'une quelconque interprétation ou dénaturation des clauses sur lesquelles ils portent. En cas de difficulté d'interprétation ou de contradiction entre le contenu d'une clause et son titre, ce dernier est réputé non écrit.

Il est expressément convenu entre les parties que la langue régissant les présentes CGU est le Français. En cas de contradiction entre les présentes CGU et les mêmes CGU traduites, les présentes CGU primeront sur celles traduites en langue étrangère.

## 2.10 Règlement alternatif des litiges avant saisine des tribunaux

En cas de litige relatif à l'interprétation, la formation ou l'exécution des CGU entre les parties à propos de l'exécution ou de l'interprétation des CGU, les parties s'engagent avant toute saisine des tribunaux à coopérer avec diligence et bonne foi afin de résoudre rapidement et par voie amiable le litige.

En cas de litige, l'utilisateur doit alors contacter l'ANTS par l'envoi d'un courrier recommandé avec accusé de réception.

Dans les deux mois suivant la réception du courrier recommandé, l'ANTS adressera une réponse écrite à l'utilisateur. Une absence de réponse de l'ANTS dans ce délai de deux mois vaut rejet.

L'utilisateur a la possibilité de répondre dans un délai de deux mois à compter de la réception de la réponse de l'ANTS.

## 2.11 Juridiction compétente et droit applicable

A défaut de règlement amiable du litige, les parties donnent compétence expresse et exclusive aux tribunaux compétents français, nonobstant la pluralité de défendeurs ou d'actions en référé ou d'appels en garantie ou de mesures conservatoires, en matière d'interprétation, contestation ou revendication concernant le Portail.

Le Portail est soumis au droit français. L'ensemble de son contenu, ses offres, ses services sont régis par la réglementation française en vigueur.

Si une disposition de ces CGU devait être tenue pour non valide ou déclarée comme telle en application d'une loi, d'un règlement ou à la suite d'une décision définitive d'une juridiction compétente les autres dispositions resteront néanmoins en vigueur.