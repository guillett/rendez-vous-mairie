export const environment = {
  production: true,
  api_url: 'http://rendez-vous-api.france-identite.fr/api',
  wss_url: 'wss://rendez-vous-api.france-identite.fr/api',
};
