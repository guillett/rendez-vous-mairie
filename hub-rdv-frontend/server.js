require('dotenv').config()
const express = require('express');
const app = express();
const port = process.env.PORT;
const path = require('path');

app.use(express.static('dist/hub-rdv-frontend/'));

app.get('*', function(req,res) {
    res.sendFile(path.resolve('dist/hub-rdv-frontend/index.html'));
});

app.listen(port, () => {
    console.log(`HUB RDV Frontend app listening on port ${port}`);
});
